# Usable Technologies: Guidelines

Let us say you are convinced, and you want some further instructions on how to help. This is that document. The first two sections are shorter and speak on how to help without developing brand new UT software. The third primary section will contain guidelines on how to maximally simplify software, in the attempt to be a UT.

## Use UT, Tell Your Friends

Empowering the people has no effect if the people do not use this ability. The truth is that *you* **are** capable of these things. If you are unhappy with the state of things in a given field, try a UT program. If you are still unhappy with it, it will be the easiest option for you to modify to your liking, so try doing that! If you see that your friends are unhappy, tell them about UT as an option. If you know developers, tell them about UT too. If you know people with similar goals to ours, tell them about UT so that they may host their own variation.

## Host Mirrors

UT is, as a fork of TT, Trivial. It is not meant to be centralized. This hosted instance, which is not original, is too, just a mirror. Your mirror may also contain changes, such that it will fit your needs and your community better. The whole thing is available under the Unlicense, so you needn't worry about anything you do with the idea. Here is the recommended procedure to make a mirror:

1. Copy the sources (whether using git or as plain files).

2. Modify the `_footer.html` file to have *your* contact data, as the maintainer.

3. Clean out the `external` page, if it exists and contains entries.

4. Make any further modifications you wish and deploy it.

In the `external` page, two things may optionally go if you so wish it. You may have a list of other known mirrors on there, in which case you may wish to stay in contact with the maintainers of them. Secondly, you may have a list of certified UT software.

**Note**: this mirror of UT has neither.

## Make UT Software

The first step is simple - you must desire for your project to be Usable. If you have such a desire, insert a phrase into your README, proclaiming to the word that your project strives to achieve the standards of the UT movement, and link to a mirror of your choosing (you should link to the landing page). The rest matters little, if you manage to achieve those standards. The rest of this are but recommendations.

**Note**: this list is incomplete. Yet the trajectory, clearly, is to make code readable, especially to the originally oppressed in tech. Consider office clerks who want to automate their spreadsheets with Python. They just want "usable" tools, while digging into your code after work. Make your software usable. Usable Technology conveys at least 5x more community value than a repo of plain code.

### Software per se

1. Comment responsibly. The "dialog" commenting style works fine, as if you are explaining what your code is doing to another person beside you. For the readers to understand your code in the first try, label out your "entry points" such as `main()`, and core parts that power your main functionality. The comments should act as a guide as if your code were a tour; add explanations where necessary.

2. Keep functions and types logical and limited. In no case should you have a function that does a great many things.

3. Keep your "utilities" in a separate file. Don't let them scatter around in your main file.

4. Minimize nesting and other flow features for practical programs. Higher Order Functions are good as long as their use is clear; your experienced readers should tell at a glance what each function is doing, without scanning up and down for logic keywords and brackets; novice coders should be able to know what it does after checking out the documentation.

5. Only add the features you personally use. Instead of adding all the features, simply leave open points into which one may add features themselves. If you do not use a feature, you do not know how one might want to use it, so instead empower them to add it themselves, for their own fork.

### Meta

1. Your commits should be minimally small, and explain why the changes in question were made.

2. Keep a changelog in your README, stating what have been changed for what reasons.

3. Minimize both the restrictions of your LICENSE file, as well as the length, for understanding it in its entirety is part of understanding your project. TT recommends the Unlicense and 0BSD; UT adds BSD-3-Clause, BSD-2-Clause, MIT, ISC and WTFPL to the list.

4. Minimize the amount of "meta-work" required. A project that uses CMake requires the reader to understand CMake. This needless context-switching greatly increases the difficulty of understanding projects, especially when the meta-work grows to thousands of lines. Prefer either very small amounts thereof (basic makefile, simple script) or languages that do not have meta-work associated with them (such as Go, where the entirety of the meta work is build-flags, the use of which should be minimal).

5. Minimize "bundled" assets. In order for users to test your software easily, they shouldn't have to download a screenshot archive 5x larger than the entire codebase. For necessary datasets, provide a method for users to download them à la carte.

6. Avoid requirements of proprietary hardware, driver, or applications. For this reason, a machine learning program could be disqualified for a UT if it uses CUDA, which in turn requires nvidia hardware.

7. You need to leave some contact for anyone reading your code who needs help, whether it's on the project page or bio. Choose one you frequently check, such as personal email or a social network handle. It would be unpleasant if you contacted a project's maintainer via an organizaiton email and never received a reply.

### Additional points that deserve emphasis

Here are additional points that usually don't cause a problem but are kinda annoying if they do.

1. GitHub is now part of Microsoft. Even back when it wasn't, it was a US company, and US laws are hostile to its "rival parties". When you work with US companies, politics come into your way when you want your technology to be accessible to anyone. Consider something else, such as gitea instances hosted by non-profit organizations, non-US companies, or even yourself, e.g. Codeberg, notabug, etc.


## Certification process

Why so much hassle for the developer and their friends? Unlike TT, UT *doesn't* require developers to show around their software, seeking approval of its simplicity: "Ah, yes, I, as someone who has never coded before, understand all these components in this repo, including how the author called external API's, how they thought of this algorithm which according to google is efficient, and how to use it myself, regardless of whether I want it. I did nothing else this weekend for a friend. I even signed up an account and gave this repo a star. I deserve a Friend of The Year." Instead, I propose **self-certification**, because as I believe, there is more good than evil. Even if someone declares their gibberish project a UT, we would *know* it isn't, and if we propagate UT enough, people would notice. It's community work.

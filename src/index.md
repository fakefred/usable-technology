# Usable Technologies

Usable Technologies (UT) is a new way to start your new recreational open-source project. It is not limited to a license to adopt. It is a philosophy, on a **per-project basis**, that maximizes the **community value** of the technology created. It is not limited to software, either, however only software is covered in the documents for lack of adequate knowledge. I encourage you to make every piece of "unprofitable" technology of maximal value to the community at an acceptable cost. After you understand its implications, you can consider adopting the Usable Technologies model for your next shower idea.

![Hey - what if I started a spin-off of TT?](img/shower-idea.webp)

In the entirety of UT, the term "developers" refers to the makers and contributors of a piece of open-source technology that adheres to UT; "users", those who incorporate the aforementioned piece of technology as a compoment of their own technology; and "end users", those who use the technology, give feedback, but don't engage in any development.

UT is no burden to the developers. A partially rewritten spin-off of [TT](https://trivial.technology/), UT is, by intent, less ambitious and requires less from developers of the technology. For example, it does not require as many meta files as TT does (NEWS, READING), but instead moves that work to existing files. Also, UT doesn't mark certain "radically" permissive licenses as "superior". Let developers, perhaps including you, pick what they like.

UT is usable to the users and friendly to the community. Licensing-related hassle should be reduced to the extent that users of UT won't have the slightest worry about it if they were to fork it and build on top.

In addition, UT makes sure technology adoption is as easy as hello world to users with at least a little experience in the corresponding field. A beginner should be able to completely understand a repo in 2 days. A professional should grasp it in a rainy afternoon. It is not meant to be a plug-and-play black box that one does not open lest they contract some ancient curse.

![Black box](img/black-box.webp)

Usable Technology is built with faith that it will be used **by the good, for the good**. Since you're here, I can safely guess you're building in your free time something like [emojo.site](https://github.com/bclindner/emojo.site), [et](https://git.szy.io/mdszy/et), or if you're more into EE, a badge for an upcoming event, not concentrated uranium. Same applies to your users all the way down to end users. That said, trust your users and set your technology free.

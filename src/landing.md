# Usable Technologies: Landing

<img src="https://img.shields.io/badge/usable-technology-blue" style="width: 400px;" />

You have likely arrived here by clicking on a link found in a project's README. That link being there means that the project strives to be what I call a "Usable Technology". Here is what that means.

Since you're here, you probably already hate proprietary software. Let's proceed to our objective.

![But I just want to get angry!](img/get-angry.webp)

*Fine.* Skip the following paragraph if you find it boring.

Copyright law was created to ensure access and promote the propagation of the useful arts. However, nowadays, it does quite the opposite. A select few control every piece of proprietary technology that you use, and you must ask, as beggars, for things essential to you or your community to be added, fixed, or not removed. Because of copyright, you may not do the modifications yourself. When you may, it is likely cost-prohibitive to do so. Further, due to the centralization of the controlling parties, the risks become great: the select few hold your information, control how you may use their product, and even if they are to be sane now, they could be bought out at any moment, for all of that to change.

Usable Technology (UT) seeks to change that. You make your project for everyone. When you adopt UT for your project, you are not its sole owner. It belongs to all. It's like the second law of thermodynamics: once you publicize your technology, you don't take it back. It is a declaration to your users that it is completely consequence-free to use your technology, since you have explicitly granted all rights to them. Likewise, for someone else's UT project, it belongs to you. You should be able to understand it easily, and you should feel free to do whatever you wish with it, approved by the authors or not. It is knowledge, in its purest, emancipated form.

In case you find the project you came from unintuitive and difficult to grasp, don't panic. It is probably not your fault. UT aims to be simple enough that anyone interested can understand it without mountains to climb. It's *not* because you're too uninformed, too uneducated, "not born for this", etc. Nobody is born for a skillset. Instead, the author may have incorrectly declared it a UT because UT is **self-certified**. No measure is taken to prevent mislabeling except for one's own virtue. Here's how to identify.

If you are new to code, it is likely you should learn more concepts about how the machine works superficially, to a practical level. This is out of scope for UT.

If you have a little experience with coding, and are actively seeking to improve, a correctly labeled UT project is a good place to start; in such case that you can't make out any of the code even with the comments, or to the extreme where there is no comment at all, the project is definitely *not* UT, as it is not usable.

If you are an experienced coder, and the code you found is bullshit to you, feel free to contact its owner and express your doubts. Let the community know if they used UT maliciously.

# Usable Technologies: Manifesto

> Read a [Simple English manifesto here](manifesto-simple.html)

A specter is haunting software — the specter of Emancipation. All of the world's Powers have entered into a holy alliance to exorcise this specter: Journal and Standards Body, Government and Judge, Media and IT Company. Those who would seek to profit from those that may need knowledge they possess, whether it be monetarily or through increased influence. Those who would profit from entire fields of endeavor remaining accessible only to the elites, barred behind red tape, jargon and time barriers.

The open scientific endeavors are ridiculed. Those teaching others to make their own medication in the face of pharmaceutical abuse, when not ignored, are hunted as dangerous criminals. The open source movement only became acceptable when that fundamentally centralizing force became the very corporations it seeks to stop.

These tools: copyright, patent, trademark laws, all serve the same purpose—to centralize knowledge, keep it in the hands and control of the original creator. We, instead, seek the opposite. We seek the emancipation of knowledge, whenever possible. This movement, while pertaining to software, is fundamentally but a component of that grander philosophy.

To allow for the knowledge of software, and the ability to modify it, to spread, we reduce as many barriers as possible. Access to modification and use of any kind is granted by way of public domain or a license with little obligations. The software itself shall be accessible, ideally understandable by a layman within 2 days. Measures should be taken towards keeping it that way proactively, and easing any potential rough patches one may run into.

We call such understandable and emancipated software "Usable Software", or perhaps a "Usable Technology", for it is usable by anybody. Like the wheel, no one may lay claim to it. Like the lever, any can understand how to use, aggregate and modify it to make something else. Even if the technology is "unprofitable", no one is prevented from profiting from the creation, but that profit shall never be allowed to get in the way of the knowledge itself.

## What is to Be Done

1. The Idea must be spread, either Usable Technologies or [Trivial Technologies](https://trivial.technology). All that you can see here is available freely, to use, modify, and distribute. Host copies, host mirrors, host modifications, tell your friends. If you desire to make significant modifications, or change the manifesto, do consider calling yourself something else, lest you be confused for us.

![Rejected names: "Smol Software", "Even More Trivial Technologies", "Unmentionable Technologies"](img/rejected-names.webp)

2. There must be projects that strive to be Usable. It is not always attainable, but the intent and the effort put into sane design will still improve the state of things. Put links in your READMEs declaring your intentions, and linking to a mirror of your choosing. Not all software need be Usable, but for any activity, there should be an alternative that is, so that one dissatisfied with the state of things may jump off of it to serve themselves.

3. The people must be informed. It is overwhelmingly common for one to believe that they cannot do things, whether it be in software or other fields of endeavor. That they are too dumb, too uneducated, too incapable to even attempt them. Show them otherwise. Encourage them otherwise. Allow the masses to rise beyond what they thought themselves capable of. I have personally demonstrated to my friends how easy it is to install modern Linux distros. If technology at large isn't for everyone yet, make yours be, one piece at a time, for it is part of a social impact that will eventually make a difference.
# Usable Technology

[![UT shield](https://img.shields.io/badge/usable-technology-blue)](https://fkfd.me/ut)

Copy the following markdown to use this badge:

```markdown
[![UT shield](https://img.shields.io/badge/usable-technology-blue)](https://fkfd.me/ut)
```

This is the repository for the website of a Usable Technology mirror.
See the [website proper](https://fkfd.me/ut) for more details!

UT is a fork of [TT](https://trivial.technology).

If this mirror doesn't suit you, feel free to make your own.
Alternatively, see [TT's EXTERNAL.md](https://github.com/TrivialTechnologies/TT/blob/master/EXTERNAL.md) file for other mirrors we're aware of!

All images on the website are licensed uncer [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/).

## Making a fork

See [Guidelines](https://fkfd.me/ut/guidelines.html) - "Host Mirrors" for what to edit.

To build, you need Perl. Run `./build.sh src dst` in the root directory. HTML files will be generated in `dst/`. You can write your own template; `_header.html`, `_footer.html` and `style.css` were all modified from the original.

To deploy static HTML, you can either use GitHub pages or alike (which requires html to be placed under `docs/`. Change `dst/` to `docs/` if so), or put them in your personal server. The operation is out of scope for UT.

If you have another favicon to use for your mirror, put your favicon in the generated directory together with the static files, and modify `src/_header.html` accordingly. Regenerate html.

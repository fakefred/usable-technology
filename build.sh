#!/bin/sh
d="$(dirname $0)"
case "$d" in
    /*) ;;
    *) d="$PWD/$d"
esac
export PATH="$PATH:$d/bin"

mkdir -p dst
ssg5 src dst "$@"

# custom automation script
echo If you forked this, made customizations and errors occur, remove or modify custom scripts in build.sh

cp ./img/*.webp ./dst/img/
